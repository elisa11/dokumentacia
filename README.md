# Elisa používateľská príručka

Systém Elisa je projekt vyvíjaný pre Fakultu Elektrotechniky a Informatiky, Slovenská Technická Univerzita v Bratislave.
Tento dokument slúži ako stručný manuál k základným funkcionalitám systému.

## Prihlásenie

Systém sa prihlasuje voči školskému LDAP serveru, v aktuálnej verzií však musí mať prihlásený užívateľ nastavené práva aby mohol systém používať.
Prihlasovacie údaje sú rovnaké ako údaje do systému AIS.

## Výber schémy

Po prihlásení užívateľ potrebuje vybrať schému s ktorou chce pracovať. Výber vykoná v menu systému, môže vybrať existujúcu alebo vytvoriť novú schému. Po výbere je ešte vyzvaný či chce importovať dáta z AISu.
Táto verzia obsahuje všetky dáta s ktorými užívateľ pracuje, teda aj dostupné predmety, ľudia, miestnosti, ústavy a pod. Pri tvorbe novej verzie schémy je nutné importovať dáta z AISu.

![verzia schemy](img/verzia_schemy.png)

## Požiadavky

V sekcií požiadavky užívateľ môže vytvárať požiadavku pre jednotlivé predmety. Každý predmet by mal mať iba jednu požiadavku.

![tvorba poziadavky](img/tvorba_poziadavky.png)

## Rozvrh

Sekcia Rozvrh slúži na tvorbu a manipuláciu s rozvrhom.

### Verzia rozvrhu

Užívateľ má možnosť vybrať si s ktorou verziou rozvrhu chce pracovať. Pozor ide o inú verziu ako verzia schémy. Verzia rozvrhu obsahuje všetky požiadavky, udalosti a ich priradené časti, miestnosti a kolízie.
Verziovanie tu slúži ako ukladanie bodu, do ktorého sa užívateľ bude môcť vrátiť pokiaľ nebude spokojný so stavom rozvrhu.

![verzia rozvrhu](img/verzia_rozvrhu.png)

### Filter

Viacúrovňový filter má 2 časti. Pokiaľ sa filtruje podľa vrchných checkboxov, vyfiltrujú sa všetky udalosti vyhovujúce danému filtru. Filtrovanie podľa miestnosti zresetuje vrchný filter, a vráti iba udalosti s priradenou miestnosťou.

![filter](img/filter.png)

### Rozvrh

Udalosti sa dajú ťahať a položiť do príšlušných polí v rozvrhu. Po uložení sa udalosť natiahne podľa jej nastaveného trvania. Kliknutím na udalosť sa zobrazia informácie z požiadavky, a taktiež možnosť priradiť miestnosť.
Pokiaľ užívateľ uloží do rozvrhu udalosť, ktorá ešte nemá priradenú miestnosť, bude vyzvaný na jej priradenie.

![rozvrh](img/rozvrh.png)

### Kolízie

Kolízie sa prepočítavajú automaticky po každom priradení udalosti. Ak je udalosť v kolízií, bude označená červeným trojuhoľníkom a tieňom okolo. Kliknutím na trojuhoľník môže užívateľ vidieť všetky kolízie v ktorých sa udalosť nachádza a môže kolízie akceptovať.
Pokiaľ sa v udalosti nachádza aspoň jedna neakceptovaná udalosť, bude stále vyznačená aj tieňom, v opačnom prípade zostane iba výstražný trojuhoľník.

### Obnovenie

Naľavo od tabuľky sa nachádza tlačidlo na obnovenie, ktoré načíta udalosti nanovo. Mienené použitie je v prípade, kedy ma užívateľ v jednej karte vyfiltrovaný rozvrh pre miestnosť a v inej karte ukladá miestnosti do rozvrhu.

## Ústav, Predmety, Miestnosti, Skupint

Tieto sekcie slúžia na kontrolu dát, ktoré boli importované z AIS databázy.

## Ľudia

V tejto sekcii môže užívateľ upravovať užívateľov v systéme a priraďovať im práva.
